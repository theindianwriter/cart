import React from   'react';

class Cartlist extends React.Component{
    constructor(props){
    super(props);
    this.state = {val: 0
    };
    }
    
    
    increment=()=>{
        this.setState((prevState)=>{
        return {val: prevState.val+1};
        });
    }
    decrement=()=>{
        this.setState((prevState)=> {
            if(prevState.val) 
            return ({val : prevState.val-1});
        });  
    }
    
    
    render(){
    return(
    <ul>
        <li> 
            <button onClick={this.increment}>{'+'}</button>
        {this.state.val ? this.state.val : "zero"} 
            <button onClick={this.decrement}>{'-'}</button>
        </li>
    </ul>);
    }
    
    }

    export default Cartlist;